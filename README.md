# vk - terminal

Lightweight VK client in terminal.



## Install

### Ubuntu

`sudo apt-get install libncurses5 -dev`

#### Arch

`sudo pacman -S ncurses`



# Q&A
 - Q:Why I use `std::size_t` instead of `int` or `unsigned short int`?

 A: [Watch this](https://www.viva64.com/ru/a/0050/)


With love from [OneTheGraph](https://gitlab.com/OneTheGraph)

