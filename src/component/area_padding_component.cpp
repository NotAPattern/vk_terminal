//
// Created by main on 29.12.2020.
//
#include <vk_terminal.hpp>

void AreaPaddingComponent::setAreaWithPadding(uint16_t areaWithPadding) { _areaWithPadding = areaWithPadding; }

uint16_t AreaPaddingComponent::getAreaWithPadding() { return _areaWithPadding; }