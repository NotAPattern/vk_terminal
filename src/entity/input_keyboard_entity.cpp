//
// Created by main on 05.01.2021.
//
#include <vk_terminal.hpp>

void InputKeyboardEntity::setInputKeyboardComponent(
    InputKeyboardComponent inputKeyboardComponent) {
  _inputKeyboardComponent = inputKeyboardComponent;
}

InputKeyboardComponent InputKeyboardEntity::getInputKeyboardComponent() {
  return _inputKeyboardComponent;
}