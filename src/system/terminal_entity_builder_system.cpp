//
// Created by main on 31.12.2020.
//
#include <cstdlib>
#include <cstring>
#include <vk_terminal.hpp>

wchar_t* TerminalEntityBuilderSystem::getWideString(const char* str) {
  const size_t cSize = strlen(str) + 1;
  wchar_t* wc = new wchar_t[cSize];
  mbstowcs(wc, str, cSize);
  wc[wcslen(wc) + 1] = '\0';
  wc[wcslen(wc) + 2] = '\0';
  return wc;
}

TerminalEntity TerminalEntityBuilderSystem::build() {
  uint16_t height, width, area;
  wchar_t* terminal_color;
  // Get height and width with Ncursesw library
  getmaxyx(stdscr, height, width);
  // Set h & w to SizeComponent
  _sizeComponent.setHeight(height);
  _sizeComponent.setWidth(width);
  // Calculate area
  area = height * width;
  // Set area
  _areaComponent.setArea(area);
  // Get terminal color supports
  terminal_color = getWideString(std::getenv("TERM"));
  _terminalColorComponent.setTerminalEnvVariable(terminal_color);
  // Build TerminalEntity and return
  return TerminalEntity(_sizeComponent, _areaComponent,
                        _terminalColorComponent);
};