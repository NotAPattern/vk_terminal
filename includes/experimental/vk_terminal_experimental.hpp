//
// Created by main on 08.01.2021.
//

#ifndef VK_TERMINAL_VK_TERMINAL_EXPERIMENTAL_HPP
#define VK_TERMINAL_VK_TERMINAL_EXPERIMENTAL_HPP

#include <ncursesw/ncurses.h>
#include <vector>
#include <components.hpp>

class Components {
 public:
  Sizes sizes{};
  Areas areas{};
  Positions positions{};
  TerminalBorders terminalBorders;
  PaddingSizes paddingSizes{};
  PaddingAreas paddingAreas{};
  PaddingPositions paddingPositions{};

};

class World {
 public:
  Components components;

  void createEntity();
};

World world();

#endif  // VK_TERMINAL_VK_TERMINAL_EXPERIMENTAL_HPP
